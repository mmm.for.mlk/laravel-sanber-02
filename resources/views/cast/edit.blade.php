@extends('master')

@section('content')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1>{{ $title }}</h1>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href={{ url("/") }}>Home</a></li>
        <li class="breadcrumb-item active">{{ $title }}</li>
      </ol>
    </div>
  </div>
</div><!-- /.container-fluid -->
@endsection

@section('main-content')
<!-- Default box -->
<div class="card">
  <div class="card-header">
    <h3 class="card-title">{{ $title }}</h3>

    <div class="card-tools">
      <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
        <i class="fas fa-minus"></i>
      </button>
      <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
        <i class="fas fa-times"></i>
      </button>
    </div>
  </div>
  <div class="card-body">
    {{-- disini --}}
    <div>
      <h2>Edit Data</h2>
          <form action="/cast/{{ $cast->id }}" method="POST">
              @csrf
              @method('PUT')
              <div class="form-group">
                  <label for="nama">Nama</label>
                  <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan Nama" value="{{ $cast->nama }}">
                  @error('nama')
                      <div class="alert alert-danger">
                          {{ $message }}
                      </div>
                  @enderror
              </div>
              <div class="form-group">
                <label for="umur">Umur</label>
                <input type="number" class="form-control" name="umur" id="umur" placeholder="Masukkan Umur" min="0" max="100" value="{{ $cast->umur }}">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
              <label for="bio">Bio</label>
              <textarea name="bio" id="bio" cols="20" rows="10" class="form-control" placeholder="Masukkan Bio">{{ $cast->bio }}</textarea>
              @error('bio')
                  <div class="alert alert-danger">
                      {{ $message }}
                  </div>
              @enderror
          </div>
              <button type="submit" class="btn btn-primary">Ubah</button>
          </form>
    </div>
  {{-- sampai sini --}}
  </div>
  <!-- /.card-body -->
  <div class="card-footer">
    Footer
  </div>
  <!-- /.card-footer-->
</div>
<!-- /.card -->    
@endsection
